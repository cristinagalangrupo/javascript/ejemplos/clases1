
/* *************************************** SELECCIÓN DE FÚTBOL *************************************** */
var SeleccionFutbol=function(datos){ // función prototipo
	this.id=null;
	this.nombre=null;
	this.apellidos=null;
	this.edad=null;

	/* Getter y setter */
	this.getId=function(){
		return this.id;
	};

	this.setId=function(v){
		this.id=v || 0;
	};


	this.getApellidos=function(){
		return this.apellidos;
	};

	this.setApellidos=function(v){
		this.apellidos=v;
	};


	this.getEdad=function(){
		return this.edad;
	};

	this.setEdad=function(v){
		this.edad=v;
	};


	this.getNombre=function(){
		return this.nombre;
	};
	this.setNombre=function(v){
		this.nombre=v;
	}

	/* Métodos públicos */
	this.concentrarse=function(){
		return "concentrándome";
	};

	this.viajar=function(){
		return "viajando";
	};

	/* Constructora */
	this.seleccionFutbol=function(datos){
		this.setId(datos.id);
		this.nombre=datos.nombre || "";
		this.apellidos=datos.apellidos || "";
		this.edad=datos.edad || 0;
	};
	this.seleccionFutbol(datos);

};

var Futbolista=function(datos){
	this.dorsal=null;
	this.demarcacion=null;

	this.getDorsal=function(){
		return this.dorsal;
	};
	this.setDorsal=function(v){
		this.dorsal=v;
	};
	this.getDemarcacion=function(){
		return this.demarcacion;
	};
	this.setDemarcacion=function(v){
		this.demarcacion=v || "";
	};


	this.jugarPartido=function(){
		return "Estoy jugando"
	};
	this.entrenar=function(){
		return "Estoy entrenando un poco"
	};

	/* Constructora */

	this.futbolista=function(datos){
		this.dorsal=datos.dorsal ||0;
		this.setDemarcacion(datos.demarcacion);
		SeleccionFutbol.call(this,datos); 
		//this.seleccionFutbol(datos);
	};

	// Creo yo la sobrecarga
	// también se puede hacer con arguments.length es 1 no está vacío ==> 
	// var datos={};  if(arguments.length==1){datos=arguments[0]} (else {datos={};}) opcional porque ya lo he declarado arriba
	if(typeof(datos)=="undefined"){
		this.futbolista({}); // si no recibe argumento le paso json vacío
	}else{
		this.futbolista(datos);	
	};
	
};
//Futbolista.prototype=new SeleccionFutbol(); //si no lo hemos preparado bien habría que pasarle vacío {}



var objeto1=new SeleccionFutbol({
	id:1,
	nombre:"ejemplo",
	apellidos:"pérez",
	edad:21});


document.write(objeto1.concentrarse());
document.write(objeto1.id);

var objeto2=new Futbolista({
	id:1,
	edad:21,
	nombre:"ejemplo",
	apellidos:"perez",
	dorsal:28,
	demarcacion: "demarcacion"
});