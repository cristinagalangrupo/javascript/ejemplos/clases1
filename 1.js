/* *************************************** SELECCIÓN DE FÚTBOL *************************************** */
var SeleccionFutbol = function(padre) { // Función prototipo
    this.id =null;
    this.nombre = null;
    this.apellidos = null;
    this.edad = null;

    this.concentrarse = function() {
        console.log("Concentrandose");
    };
    this.viajar = function() {
        console.log("Viajando");
    };


    /* Constructora */
    this.seleccionFutbol = function() {

    };
    this.seleccionFutbol(padre);

};

/* *************************************** FUTBOLISTA *************************************** */
var Futbolista = function(hijo) {
    this.dorsal = null;
    this.demarcacion = null;

    this.jugarPartido = function() {
        console.log("Jugando partido");
    };
    this.entrenar = function() {
        console.log("Entrenando");
    };


    /* Constructora */

    this.futbolista = function() {
        SeleccionFutbol.call(this.hijo);
    };
    this.futbolista(hijo);
};

/* *************************************** ENTRENADOR *************************************** */
var Entrenador = function(hijo) {
    this.idFederacion = null;

    this.dirigirPartido = function() {
        console.log("Dirigiendo partido");
    };

    this.dirigirEntrenamiento = function() {
        console.log("Dirigiendo entrenamiento");
    };

    /* Constructora */
    this.entrenador = function(hijo) {
        SeleccionFutbol.call(this.hijo);
    };
    this.entrenador(hijo);

};
/* *************************************** MASAJISTA *************************************** */
var Masajista = function(hijo) {
    this.titulacion = null;
    this.aniosExperiencia = null;

    this.darMasaje = function() {
        console.log("Dando masaje");
    };

    /* Constructora */
    this.masajista = function() {
        SeleccionFutbol.call(this.hijo);
    };
    this.masajista(hijo);
};



/* *************************************** INSTANCIAR *************************************** */
var seleccion1 = new SeleccionFutbol();
var entrenador1 = new Entrenador();
var futbolista1 = new Futbolista();
var masajista1 = new Masajista();

entrenador1.dirigirEntrenamiento();
futbolista1.jugarPartido();